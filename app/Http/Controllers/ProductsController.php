<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;

class ProductsController extends Controller
{

    public function home(){
        $items = Products::all();

        return view('dashboard.index', compact('items'));
    }


    public function apiCallProducts(){
        $items = Products::all();

        return json_decode($items);
    }
    //
    public function createProduct(Request $request){
            $data = \Validator::make($request->all() , [
                        'sku'=>'required',
                        'category'=>'required',
                        'title'=>'required|max:30',
                        'price'=>'required',
                        'description'=>'required'
            ]);

            if($data->fails()){
                return redirect('/')->withErrors($data->errors());
            }else{
                $product = new Products();
                $product->sku = $request->sku;
                $product->category = $request->category;
                $product->title = $request->title;
                $product->price = $request->price;
                $product->description = $request->description;
                $product -> createdOn = date('Y-m-d H:i:s');
                $product->save();

                return redirect('/')->with([
                    'message' => 'Product Created Successfully'
                ]);
            }
            }

    public function listProducts(){

        $items = Products::all();

        return view('dashboard.index', compact('items'));
    }

    public function editProduct(Request $request , $id){

        $data = \Validator::make($request->all() , [
            'sku'=>'required',
            'category'=>'required',
            'title'=>'required|max:30',
            'price'=>'required',
            'description'=>'required'
          ]);

          if($data->fails()){
            return redirect('/')->withErrors($data->errors());
        }else{


            $product = Products::find($id);

            $product->sku = $request->sku;
            $product->category = $request->category;
            $product->title = $request->title;
            $product->price = $request->price;
            $product->description = $request->description;
            $product -> createdOn = date('Y-m-d H:i:s');
            $product->save();

            return redirect('/')->with([
                'message' => 'Product Updated Successfully'
            ]);
        }

    }


    public function  deleteProduct($id){
        $product = Products::find($id);

        if(!empty($product)){
            $product->delete();
            return redirect('/')->with([
                'message' => 'Product Deleted Successfully'
            ]);
        }else{

            return redirect('/')->with([
                'message' => 'No Product Entry Found'
            ]);

        }
    }
}
