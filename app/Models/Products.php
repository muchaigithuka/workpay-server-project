<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    // CREATE TABLE `arrears` (
    //     `id` int(5) PRIMARY key AUTO_INCREMENT NOT NULL,
    //     `sku` varchar(50) NOT NULL,
    //     `description` varchar(50) NOT NULL,
    //     `title` varchar(50) NOT NULL,
    //     `category` varchar(50) NOT NULL,
    //     `price` double NOT NULL DEFAULT '0',
    //     `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
    //   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
      

    protected $table = 'products';
    public $timestamps= false; 
    protected $primaryKey = 'id';
    protected $fillable = ['id','sku', 'description', 'title' ,'category', 'price','createdOn'];
}
