<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductsController@home');
// Route::get('/', function () {
//     return view('dashboard.index');
// });

Route::get('/core/products/list/v1' , 'ProductsController@listProducts');
Route::post('/core/products/add/v1' , 'ProductsController@createProduct');
Route::post('/core/products/edit/v1/{id}' , 'ProductsController@editProduct');
Route::post('/core/products/delete/v1/{id}' , 'ProductsController@deleteProduct');