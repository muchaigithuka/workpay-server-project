<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Products;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
    return [
        'sku' =>$faker->sku,
        'description'=>$faker->description,
        'category'=>$faker->category,
        'price'=>$faker->numberBetween(100 , 1000),
        'title'=>$faker->title
    ];
});
