<?php

use Illuminate\Database\Seeder;
use App\Models\Products;

class ProductSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::create(array(
        // 'sku' =>$faker->sku,
        // 'description'=>$faker->description,
        // 'category'=>$faker->category,
        // 'price'=>$faker->numberBetween(100 , 1000),
        // 'title'=>$faker->title
        // ));

        $count = 100;
        factory(Products::class, $count)->create();
    }
}
