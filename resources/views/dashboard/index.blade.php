@extends('layout')
@section('content')
<div class="row">
        <div class="col-md-12">
      
          <h3 class="title-5 m-b-35">data table for items</h3>
          <div class="table-data__tool">
            <div class="table-data__tool-left">
              <div class="rs-select2--light rs-select2--md">
               <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 129px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-property-gi-container"><span class="select2-selection__rendered" id="select2-property-gi-container" title="All Properties">All Properties</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                <div class="dropDownSelect2"></div>
              </div>
              <div class="rs-select2--light rs-select2--sm">
               <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 79px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-time-u4-container"><span class="select2-selection__rendered" id="select2-time-u4-container" title="Today">Today</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                <div class="dropDownSelect2"></div>
              </div>
              <button class="au-btn-filter">
                <i class="zmdi zmdi-filter-list"></i>filters</button>
              </div>
              <div class="table-data__tool-right">
               
                <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#createproperty">
                  <i class="zmdi zmdi-plus"></i>Add  New item</button>
                  <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 93px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-type-18-container"><span class="select2-selection__rendered" id="select2-type-18-container" title="Export">Export</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                    <div class="dropDownSelect2"></div>
                  </div>
                </div>
              </div>
              <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                  <thead>
                    <tr>
                      <th>
                        <label class="au-checkbox">
                          <input type="checkbox">
                          <span class="au-checkmark"></span>
                        </label>
                      </th>
                      <th>#ID</th>
                      <th>SKU Code</th>
                      <th>Title</th>
                      <th>Price</th>
                      <th>Category</th>
                      <th>Description</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($items as $data)
                    <tr class="tr-shadow">
                       <td>
                        <label class="au-checkbox">
                          <input type="checkbox">
                          <span class="au-checkmark"></span>
                        </label>
                      </td>
              </td>
                      <td>
                        {{$data->id}}
                      </td>
                      <td class="desc">{{$data->sku}}</td>
                      <td>
                        {{$data->title}}
                      </td>
                      <td>
                          {{$data->price}}
                        </td>
                      <td>{{$data->category}}</td>
                      <td>{{$data->description}}</td>
                      <td>{{$data->created_on}}</td>
                      <td>
                        <div class="table-data-feature">
                          <button class="item btn btn-success"  data-toggle="modal"   data-target="#GSCCModall-{{$data->id}}" data-placement="top" title="" data-original-title="Edit">
                            <i class="zmdi zmdi-edit"></i>
                          </button>
                      
                          <button class="item  btn btn-danger" data-toggle="modal"   data-target="#GSCCModal-{{$data->id}}" data-placement="top" title="" data-original-title="Delete">
                            <i class="zmdi zmdi-delete"></i>
                          </button>
                        
                          <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="More">
                            <i class="zmdi zmdi-more"></i>
                          </button>
                        </div>
                      </td>
                    </tr>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
      
            </div>
          </div>
@endsection



@foreach($items as $data)
  <!--Start Edit/Show product-->
  <div id="GSCCModall-{{ $data->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <form action="{{ url('/core/products/edit/v1/'.$data->id)}}" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
          <li><a class="tooltips" data-toggle="tooltip" data-placement="top" title="Delete"> </a></li>
      <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
               <p>
               <h4  id="desc">#{{ $data->id}}</h4>
                      <h4 class="modal-title" id="myModalLabel">View / Edit ?</h4>
  
               </p>
           </div>
           <div class="modal-body">
              <form id="form-validation" name="form-validation" action="{{url('/core/products/edit/v1/'.$data->id)}}" method="post">
                <div class="form-group">
                    <label class="form-label" for="validation_pname">Product SKU :</label>
                    <input id="sku"
                           class="form-control"
                           name="sku"
                           value="{{ $data->sku }}"
                           type="text" data-validation="[L>=6, L<=18, MIXED]"
                           data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                           data-validation-regex="/^((?!admin).)*$/i"
                           data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                </div>
                <div class="form-group">
                    <label class="form-label" for="validation_pname">Product Price :</label>
                    <input id="price"
                           class="form-control"
                           name="price"
                           value="{{ $data->price }}"
                           type="text" data-validation="[L>=6, L<=18, MIXED]"
                           data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                           data-validation-regex="/^((?!admin).)*$/i"
                           data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                </div>
                  <div class="form-group">
                          <label class="form-label" for="validation_pname">Product Title :</label>
                          <input id="title"
                                 class="form-control"
                                 name="title"
                                 value="{{ $data->title }}"
                                 type="text" data-validation="[L>=6, L<=18, MIXED]"
                                 data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                                 data-validation-regex="/^((?!admin).)*$/i"
                                 data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                      </div>
                      <div class="form-group">
                          <label class="form-label" for="validation_pname">Product Category :</label>
                          <input id="category"
                                 class="form-control" 
                                 name="category"
                                 value="{{ $data->category }}"
                                 type="text" data-validation="[L>=6, L<=18, MIXED]"
                                 data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                                 data-validation-regex="/^((?!admin).)*$/i"
                                 data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                      </div>
                      <div class="form-group">
                          <label class="form-label" for="validation_pname">Product Description :</label>
                          <input id="description"
                                 class="form-control" rows="3"
                                 name="description"
                                 value="{{ $data->description }}"
                                 type="textarea" data-validation="[L>=6, L<=18, MIXED]"
                                 data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                                 data-validation-regex="/^((?!admin).)*$/i"
                                 data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                      </div>

          </div>
          <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="uid" value="{{ $data->id }}">
              <button type="button" class="btn" data-dismiss="modal">Close Window</button>
              <button type="submit" class="btn btn-primary" id="myBtn">Edit Product Info</button>
          </div>
          </form>
          </div>
       </div>
      </form>
     </div>
     @endforeach
      <!--End Edit/Show product-->



      
      
  <!--Start Delete product-->
  @foreach($items as $data)
  <div id="GSCCModal-{{$data->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <form action="{{ url('/core/products/delete/v1/'.$data->id)}}" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
          <li><a class="tooltips" data-toggle="tooltip" data-placement="top" title="Delete"> </a></li>
      <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
               <p>
               <h4  id="desc">#{{ $data->id}}</h4>
                      <h4 class="modal-title" id="myModalLabel">Delete Action !!</h4>
  
               </p>
           </div>
           <div class="modal-body">
             <p>
                  
                  <h4 id="fav-title"> Are you sure you want to delete this Product ? {{$data->title }}</h4>
             </p>
           </div>
           
           <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Cancel Action</button>
             <button type="submit" class="btn btn-primary">Continue with Delete</button>
           </div>
          </div>
       </div>
      </form>
     </div>
     @endforeach
      <!--End Delete-->

<div class="modal fade" id="createproperty" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
           <div class="col-lg-6">
                <div class="mb-5">
                    <form id="form-validation" name="form-validation" action="{{url('/core/products/add/v1')}}" method="post">
       
                        <div class="form-group">
                            <label class="form-label" for="validation_pname">SKU(Stock Keeping Unit)</label>
                            <span class="help">e.g. "UNGA-SOKO-003"</span>
                            <input id="sku"
                                   class="form-control required-field"
                                   required 
                                   name="sku"
                                   type="text" data-validation="[L>=6, L<=18, MIXED]"
                                   data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                                   data-validation-regex="/^((?!admin).)*$/i"
                                   data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="validation_pname">Title</label>
                            <span class="help">e.g. "UNGA WA SOKO"</span>
                            <input id="title"
                                   class="form-control required-field"
                                   required 
                                   name="title"
                                   type="text" data-validation="[L>=6, L<=18, MIXED]"
                                   data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                                   data-validation-regex="/^((?!admin).)*$/i"
                                   data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                        </div>
                         <div class="form-group">
                            <label class="form-label" for="validation_pdescription">Description</label>
                            <input id="description"
                            required
                                   class="form-control"
                                   name="description"
                                   type="text" data-validation="[L>=6, L<=18, MIXED]"
                                   data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                                   data-validation-regex="/^((?!admin).)*$/i"
                                   data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                        </div>

                        <div class="form-group">
                            <label class="form-label" for="validation_paddress">Category</label></label>
                            <input id="category"
                                   class="form-control"
                                   required
                                   name="category"
                                   type="text" data-validation="[L>=6, L<=18, MIXED]"
                                   data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                                   data-validation-regex="/^((?!admin).)*$/i"
                                   data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                        </div>

                        <div class="form-group">
                            <label class="form-label" for="validation_paddress">Price</label></label>
                            <input id="price"
                                   class="form-control"
                                   required
                                   name="price"
                                   type="text" data-validation="[L>=6, L<=18, MIXED]"
                                   data-validation-message="$ must be between 6 and 18 characters. No special characters allowed."
                                   data-validation-regex="/^((?!admin).)*$/i"
                                   data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
                        </div>
                        <div class="form-actions pull-right">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-success width-150">Create Product</button>
                            
                        </div>
                    </form>
                </div>

            </div>
            </div>
            </div>
            <div class="modal-footer">
               
            </div>

        </div>
    </div>
</div>


