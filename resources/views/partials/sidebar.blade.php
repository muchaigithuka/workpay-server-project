<aside class="menu-sidebar2">
  <div class="logo">
    <a href="#">
      <img src="images/logo.png" alt="Swift Cashless" />
    </a>
  </div>
  <div class="menu-sidebar2__content js-scrollbar1">
    <div class="account2">
      <div class="image img-cir img-120">
        <img src="images/icon/avatar-big-01.jpg" alt="John Doe" />
      </div>
      <h4 class="name">john doe</h4>
      <a href="#">Sign out</a>
    </div>
    <nav class="navbar-sidebar2">
      <ul class="list-unstyled navbar__list">
        <li class="active has-sub">
          <a class="js-arrow" href="{{url('/')}}">
            <i class="fas fa-tachometer-alt"></i>Dashboard
            <span class="arrow">
              <i class="fas fa-angle-down"></i>
            </span>
          </a>

        </li>
        <li>
          <a href="{{url('/stock')}}">
            <i class="fas fa-chart-bar"></i>Inventory</a>
            <span class="inbox-num">3</span>
          </li>
          <li>
            <a href="{{url('/order')}}">
              <i class="fas fa-shopping-basket"></i>Sales</a>
            </li>
            <li>
            <a href="{{url('/item')}}">
              <i class="fas fa-shopping-basket"></i>Items</a>
            </li>
            <li>
            <a href="{{url('/invoice')}}">
              <i class="fas fa-shopping-basket"></i>Invoices</a>
            </li>
            <li>
              <a href="{{url('/payment')}}">
                <i class="fas fa-shopping-basket"></i>Payments</a>
              </li>
              <li>
                <a href="{{ url('/customer')}}">
                  <i class="fas fa-shopping-basket"></i>Customers</a>
                </li>
                <li>
                  <a href="{{url('/warehouse')}}">
                    <i class="fas fa-shopping-basket"></i>Settings</a>
                  </li>


                </ul>
              </nav>
            </div>
          </aside>
