<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags-->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="au theme template">
  <meta name="author" content="Hau Nguyen">
  <meta name="keywords" content="au theme template">

  <!-- Title Page-->
  <title></title>


  <!-- Fontfaces CSS-->
  {!! Html::style("css/font-face.css") !!}
  {!! Html::style("vendor/font-awesome-4.7/css/font-awesome.min.css") !!}
  {!! Html::style("vendor/font-awesome-5/css/fontawesome-all.min.css") !!}
  {!! Html::style("vendor/mdi-font/css/material-design-iconic-font.min.css") !!}

  <!-- Bootstrap CSS-->
  {!! Html::style("vendor/bootstrap-4.1/bootstrap.min.css") !!}

  <!-- Vendor CSS-->
  {!! Html::style("vendor/animsition/animsition.min.css") !!}
  {!! Html::style("vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css") !!}
  {!! Html::style("vendor/wow/animate.css") !!}
  {!! Html::style("vendor/css-hamburgers/hamburgers.min.css") !!}
  {!! Html::style("vendor/slick/slick.css") !!}
  {!! Html::style("vendor/select2/select2.min.css") !!}
  {!! Html::style("vendor/perfect-scrollbar/perfect-scrollbar.css") !!}
  {!! Html::style("vendor/vector-map/jqvmap.min.css") !!}

  <!-- Main CSS-->
  {!! Html::style("css/theme.css") !!}

</head>

<body class="animsition">
  <div class="page-wrapper">
    <!-- MENU SIDEBAR-->
    @include('partials.sidebar')
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container2">
      <!-- HEADER DESKTOP-->
      @include('partials.header')
      @include('partials.header_sidebar')
      <!-- END HEADER DESKTOP-->

      <!-- BREADCRUMB-->
      <section class="au-breadcrumb m-t-75">
        <div class="section__content section__content--p30">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="au-breadcrumb-content">
                  <div class="au-breadcrumb-left">
                    <span class="au-breadcrumb-span">You are here:</span>
                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                      <li class="list-inline-item active">
                        <a href="#">Home</a>
                      </li>
                      <li class="list-inline-item seprate">
                        <span>/</span>
                      </li>
                      <li class="list-inline-item">Dashboard</li>
                    </ul>
                  </div>
                  @yield('cta_button')
                </div>
              </div>


            </div>
          </div>
        </section>
        <!-- END BREADCRUMB-->
        <div class="section__content section__content--p30">
          <div class="container-fluid">
              @include('notification.notify')

              @yield('content')

          </div>
        </div>

        <section>
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="copyright">
                  <p>Copyright © 2019 Swift Cashless</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- END PAGE CONTAINER-->
      </div>

    </div>

    <!-- Jquery JS-->
    {{ Html::script("vendor/jquery-3.2.1.min.js") }}
    <!-- Bootstrap JS-->
    {{ Html::script("vendor/bootstrap-4.1/popper.min.js") }}
    {{ Html::script("vendor/bootstrap-4.1/bootstrap.min.js") }}
    <!-- Vendor JS       -->
    {{ Html::script("vendor/slick/slick.min.js") }}
    {{ Html::script("vendor/wow/wow.min.js") }}
    {{ Html::script("vendor/animsition/animsition.min.js") }}
    {{ Html::script("vendor/bootstrap-progressbar/bootstrap-progressbar.min.js") }}
    {{ Html::script("vendor/counter-up/jquery.waypoints.min.js") }}
    {{ Html::script("vendor/counter-up/jquery.counterup.min.js") }}
    {{ Html::script("vendor/circle-progress/circle-progress.min.js") }}
    {{ Html::script("vendor/perfect-scrollbar/perfect-scrollbar.js") }}
    {{ Html::script("vendor/chartjs/Chart.bundle.min.js") }}
    {{ Html::script("vendor/select2/select2.min.js") }}
    {{ Html::script("vendor/vector-map/jquery.vmap.js") }}
    {{ Html::script("vendor/vector-map/jquery.vmap.min.js") }}
    {{ Html::script("vendor/vector-map/jquery.vmap.sampledata.js") }}
    {{ Html::script("vendor/vector-map/jquery.vmap.world.js") }}

    <!-- Main JS-->
    {{ Html::script("js/main.js") }}
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/a2bd7673/cloudflare-static/rocket-loader.min.js" data-cf-settings="38d6f0e881cf33103e76b0c2-|49" defer=""></script>

  </body>

  </html>
  <!-- end document-->
